/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
    const collection = new Collection({
      "id": "jcuoy3rkaebc4op",
      "created": "2024-12-04 15:04:40.746Z",
      "updated": "2024-12-04 15:04:40.746Z",
      "name": "promptsCache",
      "type": "base",
      "system": false,
      "schema": [
        {
          "system": false,
          "id": "vbiqglol",
          "name": "system",
          "type": "text",
          "required": false,
          "presentable": false,
          "unique": false,
          "options": {
            "min": null,
            "max": null,
            "pattern": ""
          }
        },
        {
          "system": false,
          "id": "ssm1ldft",
          "name": "user",
          "type": "text",
          "required": false,
          "presentable": false,
          "unique": false,
          "options": {
            "min": null,
            "max": null,
            "pattern": ""
          }
        },
        {
          "system": false,
          "id": "vqtumccd",
          "name": "variables",
          "type": "json",
          "required": false,
          "presentable": false,
          "unique": false,
          "options": {
            "maxSize": 10000000
          }
        },
        {
          "system": false,
          "id": "m0lmf7c9",
          "name": "hashCode",
          "type": "text",
          "required": true,
          "presentable": false,
          "unique": true,
          "options": {
            "min": null,
            "max": null,
            "pattern": ""
          }
        },
        {
          "system": false,
          "id": "zpc20pki",
          "name": "produced",
          "type": "relation",
          "required": false,
          "presentable": false,
          "unique": false,
          "options": {
            "collectionId": "ozvy5xqyyn1vgb4",
            "cascadeDelete": false,
            "minSelect": null,
            "maxSelect": null,
            "displayFields": null
          }
        }
      ],
      "indexes": [
        "CREATE INDEX `idx_ybF9Qae` ON `promptsCache` (`produced`)",
        "CREATE UNIQUE INDEX `idx_OWhxqGI` ON `promptsCache` (`hashCode`)"
      ],
      "listRule": null,
      "viewRule": null,
      "createRule": null,
      "updateRule": null,
      "deleteRule": null,
      "options": {}
    });
  
    return Dao(db).saveCollection(collection);
  }, (db) => {
    const dao = new Dao(db);
    const collection = dao.findCollectionByNameOrId("jcuoy3rkaebc4op");
  
    return dao.deleteCollection(collection);
  })